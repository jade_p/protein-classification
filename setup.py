from setuptools import find_namespace_packages, setup

if __name__ == '__main__':
    setup(
        name='ai_blast',
        version='1.0.0',
        description='Blast like tool using deep learning',
        author='Jade Perdereau',
        license='GPLv3',
        keywords='IA, deep-learning',
        packages=find_namespace_packages(where='src'),
        package_dir={'': 'src'},
        include_package_data=True,
        install_requires=[
                          'pandas                   >= 1.1.5',
                          'biopython                >= 1.78',
                          'numpy                    >= 1.13.3',
                          'scikit_learn             >=0.24.2',
                          'torch                    >=1.9.0',
                          'transformers             >=4.7.0',
                          'Bio >= 0.5'],
        setup_requires=['pytest-runner              >= 5.1',
                        'wheel                      >= 0.33.4',
                        'setuptools                 >= 36.2.1'],
        tests_require=['pytest                      >= 5.3',
                       'coverage                    >= 5.0'],
        extras_require={},
        entry_points={
            'console_scripts': [
                'ai_blast = ai_blast.app:main',
            ]
        },
        zip_safe=False
    )
