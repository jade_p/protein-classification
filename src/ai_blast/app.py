from __future__ import print_function
import sys
import numpy as np
from argparse import ArgumentParser, Namespace
from typing import Tuple
from Bio import SeqIO
import re
import pandas as pd
import os
import torch
from transformers import AutoTokenizer
import logging
import sys
import torch.utils.data
import torch.utils.data.distributed
from model_def import ProteinClassifier
import argparse
from pathlib import Path

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

tokenizer = AutoTokenizer.from_pretrained("Rostlab/prot_bert")
MAX_LEN = 512

use_cuda = True
device = torch.device("cuda" if use_cuda else "cpu")

cwd = os.getcwd()
dir = Path(cwd).parents[1]

def parse_cli(parameters=None) -> Namespace:
    if parameters is None:
        parameters = sys.argv[1:]
    parser = ArgumentParser(description='Blast like tools using deep-learning')
    parser.add_argument('FASTA_FILE', help='Input fasta file to parse')
    return parser.parse_args(parameters)


def fasta_iter(fasta_file: str) -> Tuple[str, str, int, int, str, str, str]:
    """A fasta file iterator which translate dna sequence to amino acid on the fly

    :param fasta_file:
    :return: A tuple seq_id, start , end, gene, species, species, genre, amino acid sequence
    """
    for _record in SeqIO.parse(fasta_file, 'fasta'):
        _fields = re.sub(r'[\:\s\-]', ':', _record.description).split(':')
        _seq_id, _gene, _start, _end, _species, _genre = _fields[0], _fields[1], _fields[2], _fields[3], _fields[4], _fields[5]
        _start = int(_start[1:]) if _start[0].isalpha() else int(_start)
        _end = int(_end)
        yield _seq_id, _gene, _start, _end, _species, _genre, _record.seq.translate()


def fasta_reader(fasta_file: str) -> pd.DataFrame:
    """Return dataframe with biological sequence
    Parameters:
    -----------
    fasta_file: str
        fasta file to read and convert to DataFrame

    Returns:
    --------
    pd.DataFrame
        Dataframe with sequences and its metadata
        ==========  ==============================================================
        seq_id      NIH sequence id
        start       gene start position
        end         gene end position
        gene        gene name
        species     species
        genre       genre
        sequence    amino acid sequences
        ==========  ==============================================================
    """

    seq_ids = []
    starts = []
    ends = []
    genes = []
    species_list = []
    genres = []
    sequences = []
    for seq_id, gene, start, end, species, genre, seq in fasta_iter(fasta_file):
        seq_ids.append(seq_id)
        starts.append(start)
        ends.append(end)
        genes.append(gene)
        species_list.append(species)
        genres.append(genre)
        sequences.append(seq)
    return pd.DataFrame({'seq_id': seq_ids,
                         'start': np.asarray(starts).astype(dtype=np.int64),
                         'ends': np.asarray(ends).astype(dtype=np.int64),
                         'gene': genes,
                         'species': species_list,
                         'genre': genres,
                         'sequence': sequences})


def load_model(model, path):
    """
    Loads model

    :param model: Model architecture
    :param path: model stat dict path
    :return: loaded model
    """
    model.load_state_dict(torch.load(path))
    model.to(device)
    model.eval()
    return model


def encode_sequence(sequence):
    """
    Encodes text sequence for inference

    :param sequence: sequence string
    :return: input id, input mask
    """
    encoded_sequence = tokenizer.encode_plus(
        sequence,
        max_length=MAX_LEN,
        add_special_tokens=True,
        return_token_type_ids=False,
        padding='max_length',
        return_attention_mask=True,
        return_tensors='pt'
    )
    input_ids = encoded_sequence['input_ids']
    attention_mask = encoded_sequence['attention_mask']
    input_id = input_ids.to(device)
    input_mask = attention_mask.to(device)
    return input_id, input_mask


def predict(input_id, input_mask, model):
    """
    Predicts pgm or pckA class on new data

    :param input_id: input id
    :param input_mask: input mask
    :param model: trained model
    :return: predicted class
    """
    output = model(input_id, input_mask)
    _, prediction = torch.max(output, dim=1)
    if prediction.item() == 0:
        predicted_class = 'pgm'
    else:
        predicted_class = 'pckA'
    return predicted_class


def main(args):
    """
    Inference: Predicts class for new input sequences as fasta file and prints predicted class in log

    :param model_path: path of trained model using train.py
    :param test_data_path: directory of fast_a test file
    :return: None
    """

    df = fasta_reader(os.path.join(dir, args.test_data_path))
    df['sequence'] = df['sequence'].apply(lambda x: ' '.join(str(x)))
    df['sequence'] = df['sequence'].apply(lambda x: x.rstrip('* '))

    model = ProteinClassifier(n_classes=2)
    model = load_model(model, args.model_path)
    for sequence in df['sequence'].values:
        input_id, input_mask = encode_sequence(sequence)
        with torch.no_grad():
            predicted_class = predict(input_id, input_mask, model)

            logger.info('The predicted gene category is : {}'.format(predicted_class))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--model-path", type=str, default='models/cv_model.pth', metavar="N", help="trained model path"
    )

    parser.add_argument(
        "--test-data-path", type=str, default='test/resources/pgm_pckA_test.fasta', metavar="NL",
                        help="input path for data file to predict"
    )
    main(parser.parse_args())



