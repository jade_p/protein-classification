from __future__ import print_function
import sys
import numpy as np
from argparse import ArgumentParser, Namespace
from typing import Tuple
from Bio import SeqIO
import re
import pandas as pd
import os
from sklearn.model_selection import train_test_split, StratifiedKFold
import torch
from transformers import AutoTokenizer, AutoModelForMaskedLM
import argparse
import logging
import sys
from torch import nn
import torch.utils.data
import torch.utils.data.distributed
from torch.utils.data import Dataset, DataLoader, RandomSampler, TensorDataset
from transformers import BertTokenizer, get_linear_schedule_with_warmup
from model_def import ProteinClassifier
from data_prep import ProteinSequenceDataset
from pathlib import Path


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

logger.info(torch.cuda.is_available())
logger.info(torch.cuda.memory_summary(device=None, abbreviated=False))

MAX_LEN = 512
PRE_TRAINED_MODEL_NAME = 'Rostlab/prot_bert_bfd_localization'
tokenizer = BertTokenizer.from_pretrained(PRE_TRAINED_MODEL_NAME, do_lower_case=False)

cwd = os.getcwd()
dir = Path(cwd).parents[1]

def parse_cli(parameters=None) -> Namespace:
    if parameters is None:
        parameters = sys.argv[1:]
    parser = ArgumentParser(description='Blast like tools using deep-learning')
    parser.add_argument('FASTA_FILE', help='Input fasta file to parse')
    return parser.parse_args(parameters)


def fasta_iter(fasta_file: str) -> Tuple[str, str, int, int, str, str, str]:
    """A fasta file iterator which translate dna sequence to amino acid on the fly

    :param fasta_file:
    :return: A tuple seq_id, start , end, gene, species, species, genre, amino acid sequence
    """
    for _record in SeqIO.parse(fasta_file, 'fasta'):
        _fields = re.sub(r'[\:\s\-]', ':', _record.description).split(':')
        _seq_id, _gene, _start, _end, _species, _genre = _fields[0], _fields[1], _fields[2], _fields[3], _fields[4], _fields[5]
        _start = int(_start[1:]) if _start[0].isalpha() else int(_start)
        _end = int(_end)
        yield _seq_id, _gene, _start, _end, _species, _genre, _record.seq.translate()


def fasta_reader(fasta_file: str) -> pd.DataFrame:
    """Return dataframe with biological sequence
    Parameters:
    -----------
    fasta_file: str
        fasta file to read and convert to DataFrame

    Returns:
    --------
    pd.DataFrame
        Dataframe with sequences and its metadata
        ==========  ==============================================================
        seq_id      NIH sequence id
        start       gene start position
        end         gene end position
        gene        gene name
        species     species
        genre       genre
        sequence    amino acid sequences
        ==========  ==============================================================
    """

    seq_ids = []
    starts = []
    ends = []
    genes = []
    species_list = []
    genres = []
    sequences = []
    for seq_id, gene, start, end, species, genre, seq in fasta_iter(fasta_file):
        seq_ids.append(seq_id)
        starts.append(start)
        ends.append(end)
        genes.append(gene)
        species_list.append(species)
        genres.append(genre)
        sequences.append(seq)
    return pd.DataFrame({'seq_id': seq_ids,
                         'start': np.asarray(starts).astype(dtype=np.int64),
                         'ends': np.asarray(ends).astype(dtype=np.int64),
                         'gene': genes,
                         'species': species_list,
                         'genre': genres,
                         'sequence': sequences})


def _get_train_data_loader(batch_size, dataset):
    """
    Encodes train data for training

    :param batch_size: batch_size for trainging
    :param dataset: training dataframe
    :return: train data loader
    """
    train_data = ProteinSequenceDataset(
        sequence=dataset.sequence.to_numpy(),
        targets=dataset.pckA.to_numpy(),
        tokenizer=tokenizer,
        max_len=MAX_LEN
    )

    train_dataloader = DataLoader(train_data, batch_size=batch_size, shuffle=False, num_workers=8, pin_memory=True)
    return train_dataloader


def _get_test_data_loader(batch_size, dataset):
    """
    Encodes test data for testing

    :param batch_size: batch_size for testing
    :param dataset: testing dataframe
    :return: test data loader
    """
    test_data = ProteinSequenceDataset(
        sequence=dataset.sequence.to_numpy(),
        targets=dataset.pckA.to_numpy(),
        tokenizer=tokenizer,
        max_len=MAX_LEN
    )
    test_sampler = RandomSampler(test_data)
    test_dataloader = DataLoader(test_data, sampler=test_sampler, batch_size=batch_size)
    return test_dataloader


def freeze(model, frozen_layers):
    """
    Freeze the selected layers of the pretrained model

    :param model: trained pytorch model
    :param frozen layers: index of the last frozen layer
    :return: None
    """
    modules = [model.bert.encoder.layer[:frozen_layers]]
    for module in modules:
        for param in module.parameters():
            param.requires_grad = False


def train(args):
    """
    Trains the model on the training data using 5-fold cross validation, tests it on validation data and saves it after training

    :params : defined in parser
    :return: None
    """
    use_cuda = True
    device = torch.device("cuda" if use_cuda else "cpu")

    torch.manual_seed(args.seed)
    if use_cuda:
        torch.cuda.manual_seed(args.seed)

    df = fasta_reader(os.path.join(dir, args.train_data_path))
    df['sequence'] = df['sequence'].apply(lambda x: ' '.join(str(x)))
    df['sequence'] = df['sequence'].apply(lambda x: x.rstrip('* '))
    output = df['gene']
    output = pd.get_dummies(output)

    k_folds = 5
    kfold = StratifiedKFold(n_splits=k_folds, shuffle=True)
    # the model will be a pckA-detector so 0 if pgm and 1 if pcka
    y = output['pckA']
    X = df['sequence']
    model = ProteinClassifier(
        n_classes=2
    )
    freeze(model, args.frozen_layers)

    model = model.to(device)

    optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.9)

    loss_fn = nn.CrossEntropyLoss().to(device)
    for train_index, test_index in kfold.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        df_train = pd.concat([X_train, y_train], axis=1)
        df_test = pd.concat([X_test, y_test], axis=1)

        train_loader = _get_train_data_loader(args.batch_size, df_train)

        test_loader = _get_test_data_loader(args.test_batch_size, df_test)

        total_steps = len(train_loader.dataset)

        scheduler = get_linear_schedule_with_warmup(
            optimizer,
            num_warmup_steps=0,
            num_training_steps=total_steps)

        logger.info("Max length of sequence: ".format(str(MAX_LEN)))
        logger.info("Freezing {} layers".format(args.frozen_layers))
        logger.info("Model used: ".format(str(PRE_TRAINED_MODEL_NAME)))

        logger.debug(
            "Processes {}/{} ({:.0f}%) of train data".format(
                len(train_loader.sampler),
                len(train_loader.dataset),
                100.0 * len(train_loader.sampler) / len(train_loader.dataset),
            )
        )

        for epoch in range(1, args.epochs + 1):

            model.train()

            for step, batch in enumerate(train_loader):
                b_input_ids = batch['input_ids'].to(device)
                b_input_mask = batch['attention_mask'].to(device)
                b_labels = batch['targets'].to(device)

                outputs = model(b_input_ids, attention_mask=b_input_mask)
                loss = loss_fn(outputs, b_labels)

                loss.backward()
                torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

                optimizer.step()
                optimizer.zero_grad()

                if step % args.log_interval == 0:
                    logger.info(
                        "Collecting data from Master Node: \n Train Epoch: {} [{}/{} ({:.0f}%)] Training Loss: {:.6f}".format(
                            epoch,
                            step * len(batch['input_ids']),
                            len(train_loader.dataset),
                            100.0 * step / len(train_loader),
                            loss.item(),
                        )
                    )
            test(model, test_loader, device)
            scheduler.step()
            model_save = model.module if hasattr(model, "module") else model
            save_model(model_save, args.model_dir)
            torch.cuda.empty_cache()


def save_model(model, model_dir):
    """
    A model saving method

    :param model: pytorch trained model
    :param model_dir: directory to store the model
    :return: None
    """
    path = os.path.join(model_dir, 'cv_model.pth')
    torch.save(model.state_dict(), path)
    logger.info(f"Saving model: {path} \n")


def test(model, test_loader, device):
    """
    A method to test the model at each epoch on the validation data

    :param model: trained pytorch model
    :param test_loader: test loader
    :param device: computing device
    :prints: confusion matrix for each class (index 0 for pgm and 1 for pckA), validation loss and valisation accuracy
    :return: None
    """
    model.eval()
    losses = []
    correct_predictions = 0
    loss_fn = nn.CrossEntropyLoss().to(device)
    confusion_matrix = torch.zeros(2, 2)
    with torch.no_grad():
        for batch in test_loader:
            b_input_ids = batch['input_ids'].to(device)
            b_input_mask = batch['attention_mask'].to(device)
            b_labels = batch['targets'].to(device)

            outputs = model(b_input_ids, attention_mask=b_input_mask)
            _, preds = torch.max(outputs, dim=1)
            loss = loss_fn(outputs, b_labels)
            correct_predictions += torch.sum(preds == b_labels)
            losses.append(loss.item())
            for t, p in zip(b_labels.view(-1), preds.view(-1)):
                confusion_matrix[t.long(), p.long()] += 1
    logger.info(confusion_matrix)
    logger.info('\nTest set: Validation loss: {:.4f}, Validation Accuracy: {:.0f}%\n'.format(
        np.mean(losses),
        100. * correct_predictions.double() / len(test_loader.dataset)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Data and model checkpoints directories
    parser.add_argument(
        "--batch-size", type=int, default=1, metavar="N", help="input batch size for training (default: 1)"
    )
    parser.add_argument(
        "--test-batch-size", type=int, default=1, metavar="N", help="input batch size for testing (default: 1)"
    )
    parser.add_argument("--epochs", type=int, default=2, metavar="N", help="number of epochs to train (default: 2)")
    parser.add_argument("--lr", type=float, default=0.3e-5, metavar="LR", help="learning rate (default: 0.3e-5)")
    parser.add_argument("--weight_decay", type=float, default=0.01, metavar="M", help="weight_decay (default: 0.01)")
    parser.add_argument("--seed", type=int, default=43, metavar="S", help="random seed (default: 43)")
    parser.add_argument("--epsilon", type=int, default=1e-8, metavar="EP", help="random seed (default: 1e-8)")
    parser.add_argument("--frozen_layers", type=int, default=10, metavar="NL",
                        help="number of frozen layers(default: 10)")
    parser.add_argument(
        "--log-interval",
        type=int,
        default=10,
        metavar="N",
        help="how many batches to wait before logging training status",
    )
    parser.add_argument(
        "--train-data-path", type=str, default='test/resources/pgm_pckA_train.fasta '
    )

    parser.add_argument(
        "--model-dir", type=str, default='models'
    )

    train(parser.parse_args())



