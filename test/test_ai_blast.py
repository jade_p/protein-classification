import pytest
from os import path
from pkg_resources import resource_filename
from ai_blast.app import fasta_reader


@pytest.mark.parametrize('fasta_filepath', ['pgm_pckA.fasta'])
def test_fasta_reader(fasta_filepath):
    fasta_filepath = resource_filename(__name__, path.join('resources', fasta_filepath))
    df = fasta_reader(fasta_filepath)
    assert df is not None
