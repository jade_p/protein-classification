This project is a binary classification project on the gene of DNA sequences.
It uses ProtBert model as a pretrained model. This model is based on Bert model and was pretrained on a large corpus of protein sequences in a self-supervised fashion.
The training has been performed on a machine using GPU with 8GB and 30GB memory.
The 33 DNA sequences have been randomly split into training (29/33) and test files (4/33) namely pgm_pckA_train.fasta and pgm_pckA_test.fasta.
Training is performed on the training file using cross validation and inference is done on the testing file.


## Training
To perform training, move to the src directory and run :
python3 train.py

The parameters can be changed, such as


--batch-size, input batch size for training (default: 1)
--test-batch-size, input batch size for testing (default: 8)
--epochs, number of epochs to train (default: 2)
--lr, learning rate (default: 0.3e-5)
--weight_decay, weight_decay (default: 0.01)
--seed, random seed (default: 43)
--epsilon, random seed (default: 1e-8)
--frozen_layers, number of frozen layers(default: 10)
--log-interval", how many batches to wait before logging training status
--model-dir", directory to save the model
--train-data-path, directory of training data file default 'test/resources/pgm_pckA_train.fasta

## Inference
Inference is performed on unseen data. Is it ingested as a fasta file.
To perform inference, move to the src directory and run :
python3 app.py 


The parameters can be changed, such as


--model-dir, directory of the saved model
--test-data-path,  directory of testing data file default 'test/resources/pgm_pckA_test.fasta



Note 1 : The model is a pckA detector
Note 2 : The length of the amino acid sequences selected is 512, which achieves 100% accuracy on this set for each fold.
Note 3 : The trained model has been placed in src/models/cv_model.pth. It can be trained using the training script train.py
